/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     06/05/2015 21:59:25                          */
/*==============================================================*/


drop table if exists empresas;

drop table if exists ferramentas;

drop table if exists grupos;

drop table if exists grupos_usuarios;

drop table if exists locacoes;

drop table if exists locacoes_itens;

drop table if exists portal_menus;

drop table if exists portal_modulos;

drop table if exists portal_permissoes;

drop table if exists usuarios;

/*==============================================================*/
/* Table: empresas                                              */
/*==============================================================*/
create table empresas
(
   i_empresa            integer(2) not null auto_increment,
   nome                 varchar(100) not null,
   dt_sistema           timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (i_empresa)
);

INSERT INTO empresas(i_empresa, nome) VALUES(1, 'Empresa Teste');

/*==============================================================*/
/* Table: ferramentas                                           */
/*==============================================================*/
create table ferramentas
(
   i_empresa            integer(2) not null,
   i_ferramenta         integer(8) not null,
   codigo_barras        varchar(100),
   descricao            varchar(100) not null,
   fabricante           varchar(100),
   modelo               varchar(100),
   numero_serie         varchar(50),
   patrimonio           varchar(50),
   i_usuario            integer(3),
   dt_sistema           timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (i_empresa, i_ferramenta)
);

INSERT INTO ferramentas(i_empresa, i_ferramenta, codigo_barras, descricao, fabricante, modelo, numero_serie, patrimonio)
VALUES(1, 1, '112233', 'Descrição primeira ferramenta', 'Fabricante X', 'Modelo Y', 'S1', '0001');

INSERT INTO ferramentas(i_empresa, i_ferramenta, codigo_barras, descricao, fabricante, modelo, numero_serie, patrimonio)
VALUES(1, 2, '445566', 'Descrição segunda ferramenta', 'Fabricante W', 'Modelo T', 'S1', '0002');

/*==============================================================*/
/* Table: grupos                                                */
/*==============================================================*/
create table grupos
(
   i_empresa            integer(2) not null,
   i_grupo              integer(2) not null,
   nome                 varchar(50) not null,
   status               enum('A', 'I') not null,
   i_usuario            integer(3) not null,
   dt_sistema           timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (i_empresa, i_grupo)
);

/*==============================================================*/
/* Table: grupos_usuarios                                       */
/*==============================================================*/
create table grupos_usuarios
(
   i_empresa            integer(2) not null,
   i_grupo              integer(2) not null,
   i_usuario            integer(3) not null,
   dt_sistema           timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (i_empresa, i_grupo, i_usuario)
);

/*==============================================================*/
/* Table: locacoes                                              */
/*==============================================================*/
create table locacoes
(
   i_empresa            integer(2) not null,
   i_locacao            integer(8) not null,
   dt_retirada          datetime not null,
   dt_prevista          datetime,
   dt_entrega           datetime,
   observacao           varchar(1000),
   status               enum('A', 'P', 'F', 'E', 'C') not null default 'A' comment 'A = Aberto / P = Pendente / F = Finalizada / E = Entregue / C = Cancelada',
   i_usuario_locacao    integer(3) not null,
   i_usuario            integer(3),
   dt_sistema           timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (i_empresa, i_locacao)
);

/*==============================================================*/
/* Table: locacoes_itens                                        */
/*==============================================================*/
create table locacoes_itens
(
   i_empresa            integer(2) not null,
   i_locacao            integer(8) not null,
   item                 integer not null,
   i_ferramenta         integer(8),
   status               enum('A', 'I'),
   i_usuario            integer(3) not null,
   dt_sistema           timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (i_empresa, i_locacao, item)
);

/*==============================================================*/
/* Table: portal_menus                                          */
/*==============================================================*/
create table portal_menus
(
   i_empresa            integer(2) not null,
   i_modulo             integer(2) not null,
   i_menu               integer(2) not null,
   nome                 varchar(50) not null,
   link                 varchar(200),
   ordem                integer(3) not null,
   status               enum('A', 'I') not null,
   i_usuario            integer(3) not null,
   dt_sistema           timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (i_empresa, i_modulo, i_menu)
);

INSERT INTO portal_menus(i_empresa, i_modulo, i_menu, nome, link, ordem, status, i_usuario)
VALUES(1, 1, 1, 'Modulos', 'portal_modulos/index', 10, 'A', 1);

INSERT INTO portal_menus(i_empresa, i_modulo, i_menu, nome, link, ordem, status, i_usuario)
VALUES(1, 1, 2, 'Usuários', 'usuarios/index', 20, 'A', 1);

INSERT INTO portal_menus(i_empresa, i_modulo, i_menu, nome, link, ordem, status, i_usuario)
VALUES(1, 1, 3, 'Grupos', 'grupos/index', 30, 'A', 1);

INSERT INTO portal_menus(i_empresa, i_modulo, i_menu, nome, link, ordem, status, i_usuario)
VALUES(1, 1, 4, 'Permissões', 'portal_permissoes/index', 40, 'A', 1);

INSERT INTO portal_menus(i_empresa, i_modulo, i_menu, nome, link, ordem, status, i_usuario)
VALUES(1, 1, 5, 'Ferramentas', 'ferramentas/index', 50, 'A', 1);

/*==============================================================*/
/* Table: portal_modulos                                        */
/*==============================================================*/
create table portal_modulos
(
   i_empresa            integer(2) not null,
   i_modulo             integer(2) not null,
   nome                 varchar(50) not null,
   link                 varchar(200),
   ordem                integer(3) not null,
   status               enum('A', 'I') not null,
   i_usuario            integer(3) not null,
   dt_sistema           timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (i_empresa, i_modulo)
);

INSERT INTO portal_modulos(i_empresa, i_modulo, nome, link, ordem, status, i_usuario) VALUES(1, 1, 'Cadastros', '#', 10, 'A', 1);
INSERT INTO portal_modulos(i_empresa, i_modulo, nome, link, ordem, status, i_usuario) VALUES(1, 2, 'Locações', 'locacoes/index', 20, 'A', 1);

/*==============================================================*/
/* Table: portal_permissoes                                     */
/*==============================================================*/
create table portal_permissoes
(
   i_empresa            integer(2) not null,
   i_permissao          integer(3) not null,
   i_modulo             integer(2) not null,
   i_menu               integer(2),
   tipo                 enum('U', 'G') not null comment 'U = Usuario Fixo / G = Grupo',
   i_grupo              integer(2),
   i_usuario_fixo       integer,
   libera_usuario_fixo  enum('L', 'B') comment 'L = Liberado / B = Bloqueado',
   i_usuario            integer(3) not null,
   dt_sistema           timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (i_empresa, i_permissao)
);

INSERT INTO portal_permissoes(i_empresa, i_permissao, i_modulo, i_menu, tipo, i_grupo, i_usuario_fixo, libera_usuario_fixo, i_usuario)
VALUES(1, 1, 1, 1, 'U', NULL, 1, 'L', 1);

INSERT INTO portal_permissoes(i_empresa, i_permissao, i_modulo, i_menu, tipo, i_grupo, i_usuario_fixo, libera_usuario_fixo, i_usuario)
VALUES(1, 2, 1, 2, 'U', NULL, 1, 'L', 1);

INSERT INTO portal_permissoes(i_empresa, i_permissao, i_modulo, i_menu, tipo, i_grupo, i_usuario_fixo, libera_usuario_fixo, i_usuario)
VALUES(1, 3, 1, 3, 'U', NULL, 1, 'L', 1);

INSERT INTO portal_permissoes(i_empresa, i_permissao, i_modulo, i_menu, tipo, i_grupo, i_usuario_fixo, libera_usuario_fixo, i_usuario)
VALUES(1, 4, 1, 4, 'U', NULL, 1, 'L', 1);

INSERT INTO portal_permissoes(i_empresa, i_permissao, i_modulo, i_menu, tipo, i_grupo, i_usuario_fixo, libera_usuario_fixo, i_usuario)
VALUES(1, 5, 1, 5, 'U', NULL, 1, 'L', 1);

INSERT INTO portal_permissoes(i_empresa, i_permissao, i_modulo, i_menu, tipo, i_grupo, i_usuario_fixo, libera_usuario_fixo, i_usuario)
VALUES(1, 6, 2, NULL, 'U', NULL, 1, 'L', 1);

INSERT INTO portal_permissoes(i_empresa, i_permissao, i_modulo, i_menu, tipo, i_grupo, i_usuario_fixo, libera_usuario_fixo, i_usuario)
VALUES(1, 7, 1, 2, 'U', NULL, 2, 'L', 1);

INSERT INTO portal_permissoes(i_empresa, i_permissao, i_modulo, i_menu, tipo, i_grupo, i_usuario_fixo, libera_usuario_fixo, i_usuario)
VALUES(1, 8, 1, 3, 'U', NULL, 2, 'L', 1);

INSERT INTO portal_permissoes(i_empresa, i_permissao, i_modulo, i_menu, tipo, i_grupo, i_usuario_fixo, libera_usuario_fixo, i_usuario)
VALUES(1, 9, 1, 4, 'U', NULL, 2, 'L', 1);

INSERT INTO portal_permissoes(i_empresa, i_permissao, i_modulo, i_menu, tipo, i_grupo, i_usuario_fixo, libera_usuario_fixo, i_usuario)
VALUES(1, 10, 1, 5, 'U', NULL, 2, 'L', 1);

INSERT INTO portal_permissoes(i_empresa, i_permissao, i_modulo, i_menu, tipo, i_grupo, i_usuario_fixo, libera_usuario_fixo, i_usuario)
VALUES(1, 11, 2, NULL, 'U', NULL, 2, 'L', 1);

/*==============================================================*/
/* Table: usuarios                                              */
/*==============================================================*/
create table usuarios
(
   i_empresa            integer(2) not null,
   i_usuario            integer(3) not null,
   nome                 varchar(100) not null,
   email                varchar(300) not null,
   senha                varchar(8) not null,
   status               enum('A', 'I') default 'A',
   admin                tinyint not null default 0,
   dt_sistema           timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (i_usuario, i_empresa)
);

INSERT INTO usuarios(i_empresa, i_usuario, nome, email, senha, admin)
VALUES(1, 1, 'root', 'root@root.com', '123123', 1);

INSERT INTO usuarios(i_empresa, i_usuario, nome, email, senha, admin)
VALUES(1, 2, 'admin', 'admin@admin.com', '123123', 1);

alter table ferramentas add constraint FK_reference_2 foreign key (i_empresa)
      references empresas (i_empresa) on delete restrict on update restrict;

alter table grupos add constraint FK_reference_9 foreign key (i_empresa)
      references empresas (i_empresa) on delete restrict on update restrict;

alter table grupos_usuarios add constraint FK_reference_4 foreign key (i_empresa, i_grupo)
      references grupos (i_empresa, i_grupo) on delete restrict on update restrict;

alter table grupos_usuarios add constraint FK_reference_5 foreign key (i_usuario, i_empresa)
      references usuarios (i_usuario, i_empresa) on delete restrict on update restrict;

alter table locacoes add constraint FK_reference_15 foreign key (i_usuario_locacao, i_empresa)
      references usuarios (i_usuario, i_empresa) on delete restrict on update restrict;

alter table locacoes_itens add constraint FK_reference_13 foreign key (i_empresa, i_locacao)
      references locacoes (i_empresa, i_locacao) on delete restrict on update restrict;

alter table locacoes_itens add constraint FK_reference_14 foreign key (i_empresa, i_ferramenta)
      references ferramentas (i_empresa, i_ferramenta) on delete restrict on update restrict;

alter table portal_menus add constraint FK_reference_3 foreign key (i_empresa, i_modulo)
      references portal_modulos (i_empresa, i_modulo) on delete restrict on update restrict;

alter table portal_modulos add constraint FK_reference_10 foreign key (i_empresa)
      references empresas (i_empresa) on delete restrict on update restrict;

alter table portal_permissoes add constraint FK_reference_11 foreign key (i_usuario_fixo, i_grupo)
      references usuarios (i_usuario, i_empresa) on delete restrict on update restrict;

alter table portal_permissoes add constraint FK_reference_12 foreign key (i_empresa, i_modulo)
      references portal_modulos (i_empresa, i_modulo) on delete restrict on update restrict;

alter table portal_permissoes add constraint FK_reference_6 foreign key (i_empresa, i_modulo, i_menu)
      references portal_menus (i_empresa, i_modulo, i_menu) on delete restrict on update restrict;

alter table portal_permissoes add constraint FK_reference_7 foreign key (i_empresa, i_grupo)
      references grupos (i_empresa, i_grupo) on delete restrict on update restrict;

alter table usuarios add constraint FK_reference_1 foreign key (i_empresa)
      references empresas (i_empresa) on delete restrict on update restrict;


DELIMITER $$
CREATE TRIGGER tbi_ferramentas BEFORE INSERT ON ferramentas 
FOR EACH ROW BEGIN
SET @newID = (SELECT IFNULL(MAX(i_ferramenta),0)+1 FROM ferramentas WHERE i_empresa=NEW.i_empresa);
SET NEW.i_ferramenta = @newID;
SET @lastInsertId_ferramentas = NEW.i_ferramenta;
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER tbi_grupos BEFORE INSERT ON grupos
FOR EACH ROW BEGIN
SET @newID = (SELECT IFNULL(MAX(i_grupo),0)+1 FROM grupos WHERE i_empresa=NEW.i_empresa);
SET NEW.i_grupo = @newID;
SET @lastInsertId_grupos = NEW.i_grupo;
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER tbi_locacoes BEFORE INSERT ON locacoes
FOR EACH ROW BEGIN
SET @newID = (SELECT IFNULL(MAX(i_locacao),0)+1 FROM locacoes WHERE i_empresa=NEW.i_empresa);
SET NEW.i_locacao = @newID;
SET @lastInsertId_locacoes = NEW.i_locacao;
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER tbi_locacoes_itens BEFORE INSERT ON locacoes_itens
FOR EACH ROW BEGIN
SET @newID = (SELECT IFNULL(MAX(i_locacao),0)+1 FROM locacoes_itens WHERE i_empresa=NEW.i_empresa AND i_locacao=NEW.i_locacao);
SET NEW.item = @newID;
SET @lastInsertId_locacoes_itens = NEW.item;
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER tba_portal_menus AFTER INSERT ON portal_menus
FOR EACH ROW BEGIN
INSERT INTO portal_permissoes(i_empresa, i_modulo, i_menu, tipo, i_usuario_fixo, libera_usuario_fixo, i_usuario)
VALUES(NEW.i_empresa, New.i_modulo, New.i_menu, 'U', 1, 'L', New.i_usuario);
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER tbi_portal_menus BEFORE INSERT ON portal_menus
FOR EACH ROW BEGIN
SET @newID = (SELECT IFNULL(MAX(i_menu),0)+1 FROM portal_menus WHERE i_empresa=NEW.i_empresa AND i_modulo=NEW.i_modulo);
SET NEW.i_menu = @newID;
SET @lastInsertId_portal_menus = NEW.i_menu;
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER tba_portal_modulos AFTER INSERT ON portal_modulos
FOR EACH ROW BEGIN
INSERT INTO portal_permissoes(i_empresa, i_modulo, tipo, i_usuario_fixo, libera_usuario_fixo, i_usuario)
VALUES(NEW.i_empresa, New.i_modulo, 'U', 1, 'L', New.i_usuario);
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER tbi_portal_modulos BEFORE INSERT ON portal_modulos 
FOR EACH ROW BEGIN
SET @newID = (SELECT IFNULL(MAX(i_modulo),0)+1 FROM portal_modulos WHERE i_empresa=NEW.i_empresa);
SET NEW.i_modulo = @newID;
SET @lastInsertId_portal_modulos = NEW.i_modulo;
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER tbi_portal_permissoes BEFORE INSERT ON portal_permissoes
FOR EACH ROW BEGIN
SET @newID = (SELECT IFNULL(MAX(i_permissao),0)+1 FROM portal_permissoes WHERE i_empresa=NEW.i_empresa);
SET NEW.i_permissao = @newID;
SET @lastInsertId_portal_permissoes = NEW.i_permissao;
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER tbi_usuarios BEFORE INSERT ON usuarios
FOR EACH ROW BEGIN
SET @newID = (SELECT IFNULL(MAX(i_usuario),0)+1 FROM usuarios WHERE i_empresa=NEW.i_empresa);
SET NEW.i_usuario = @newID;
SET @lastInsertId_usuarios = NEW.i_usuario;
END;
$$
DELIMITER ;

