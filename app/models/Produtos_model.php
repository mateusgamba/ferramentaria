<?php

class Produtos_model extends Crud_model
{
    public $tabela = 'produtos';

    public $chave = array('i_empresa', 'i_produto');

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        $this->db->select('i_produto, codigo, nome');
        $this->db->from('produtos');
        $this->db->where('i_empresa', $this->session->userdata('i_empresa'));
        $this->db->order_by('nome', 'ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }
}
