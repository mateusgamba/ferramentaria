<?php

class Locacoes_model extends Crud_model
{
    public $tabela = 'locacoes';

    public $chave = array('i_empresa', 'i_locacao');

    public function __construct()
    {
        parent::__construct();
    }

    public function get_locacoes($status = 'F')
    {
    	$sql = "SELECT i_locacao
                    , dt_retirada
                    , dt_prevista
                    , (SELECT nome FROM usuarios AS z WHERE z.i_empresa = a.i_empresa AND z.i_usuario = a.i_usuario) AS nome_usuario
                 FROM locacoes AS a
                 WHERE i_empresa = {$this->session->userdata('i_empresa')}
                 ORDER BY dt_prevista ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }
}
