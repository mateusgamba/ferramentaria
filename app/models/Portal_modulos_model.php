<?php

class Portal_modulos_model extends Crud_model
{
    public $tabela = 'portal_modulos';

    public $chave = array('i_empresa', 'i_modulo');

    public function __construct()
    {
        parent::__construct();
    }

    public function get_modulos($status = '')
    {
        $this->db->select('i_modulo, nome, link, ordem');
        $this->db->from('portal_modulos');
        $this->db->where('i_empresa', 1);
        if ($status!=''){
            $this->db->where('status', 'A');
        }
        $this->db->order_by('ordem', 'ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    public function get_modulos_menus_topo()
    {
        $this->load->model('Portal_menus_model');
        $lista_modulos = $this->get_modulos('A');
        $lista_menus = $this->Portal_menus_model->get_menus_topo();
        foreach ($lista_modulos as $a)
        {
            $data[$a['i_modulo']] = array('nome' => $a['nome'], 'link' => $a['link']);
            foreach ($lista_menus as $key => $val)
            {
                if ((int)$a['i_modulo'] === (int)$key)
                {
                    foreach ($val as $key1 => $val1)
                    {
                        $data[$a['i_modulo']]['menus'][$key1] = array('i_menu' => $val1['i_menu'], 'nome' => $val1['nome'], 'link' => $val1['link']);
                    }
                }
            }
        }
        return $data;
    }
}
