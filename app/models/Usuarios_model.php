<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends Crud_model
{
    public $tabela = 'usuarios';

    public $chave = array('i_empresa', 'i_usuario');

    public function __construct()
    {
        parent::__construct();
    }

	public function get_usuario($email, $senha)
	{
		$this->db->select('i_usuario, nome');
		$this->db->from('usuarios');
		$this->db->where('email', $email);
		$this->db->where('senha', $senha);
		$query = $this->db->get();
		$obj = $query->row_array();
		$query->free_result();
		return $obj;
	}

	public function get_usuarios()
	{
		$query = $this->db->get_where('usuarios', array('i_empresa' => $this->session->userdata('i_empresa')));
		$result = $query->result_array();
		$query->free_result();
		return $result;
	}

    public function get_permissoes($data_subquery = array())
    {
        $this->db->select('i_usuario, nome');
		$this->db->from('usuarios');
		$this->db->where('i_empresa', $this->session->userdata('i_empresa'));
		$this->db->where('admin', 0);
		$sub = $this->subquery->start_subquery('where_exists');
		$sub->select('1')->from('portal_permissoes');
		$sub->where('portal_permissoes.i_empresa = usuarios.i_empresa');
		$sub->where('portal_permissoes.i_usuario_fixo = usuarios.i_usuario');
		$sub->where($data_subquery);
		$this->subquery->end_subquery('', FALSE);
		$this->db->order_by('nome', 'ASC');
		$query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    public function getUsuariosFiltro($valor)
    {
        $this->db->select('i_usuario, nome');
		$this->db->from('usuarios');
		$this->db->where('i_empresa', $this->session->userdata('i_empresa'));
		$this->db->where('admin', 0);
		$this->db->like('nome', $valor);
		$this->db->order_by('nome', 'ASC');
		$query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }
}
