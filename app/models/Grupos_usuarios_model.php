<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupos_usuarios_model extends Crud_model
{
    public $tabela = 'grupos_usuarios';

    public $chave = array('i_empresa', 'i_grupo', 'i_usuario');

    public function __construct()
    {
        parent::__construct();
    }

    public function salvar($data = array())
    {
    	$this->db->insert($this->tabela, $data);
    }

    public function delete($data = array())
    {
    	$this->db->delete($this->tabela, $data);
    }

	public function get_usuarios_permissoes($i_grupo = '')
	{
		$sql = "SELECT i_usuario
					, nome
					, IFNULL((SELECT 1 FROM grupos_usuarios AS z WHERE z.i_empresa = a.i_empresa AND z.i_empresa = {$i_grupo} AND z.i_usuario = a.i_usuario),0) AS pertence
				FROM usuarios AS a
				WHERE i_empresa = {$this->session->userdata('i_empresa')}";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		$query->free_result();
		return $result;		
	}
}