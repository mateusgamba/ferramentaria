<?php if ( ! defined('BASEPATH')) exit('Não é permitido acesso direto ao Script (Portal_modulos_model)');

class Portal_menus_model extends Crud_model
{
    public $tabela = 'portal_menus';

    public $chave = array('i_empresa', 'i_modulo', 'i_menu');

    public function __construct()
    {
        parent::__construct();
    }

	public function get_menus($i_modulo = '')
	{
		$this->db->select('i_menu, nome, ordem');
		$this->db->from('portal_menus');
		$this->db->where(
						array(
							'i_empresa' => $this->session->userdata('i_empresa'),
							'i_modulo' => $i_modulo
							)
						);
		$this->db->order_by('ordem', 'asc');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function get_menus_topo()
	{
		$this->db->select('i_modulo, i_menu, nome, link, ordem');
		$this->db->from('portal_menus');
		$this->db->where(
						array(
							'i_empresa' => $this->session->userdata('i_empresa'),
							'status' => 'A'
							)
						);
		$this->db->order_by('ordem', 'asc');
		$query = $this->db->get();
		foreach($query->result_array() as $a)
		{
			$data[$a['i_modulo']][$a['ordem']] = array('i_menu' => $a['i_menu'], 'nome' => $a['nome'], 'link' => $a['link']);
		}
		$query->free_result();
		return $data;
	}

}