<?php

class Portal_permissoes_model extends Crud_model
{
    public $tabela = 'portal_permissoes';

    public $chave = array('i_empresa', 'i_permissao');

    public function __construct()
    {
        parent::__construct();
    }

    public function get_permissoes_menu($i_modulo, $i_menu)
    {
        $filtro = ' AND i_menu IS NULL';
        if ($i_menu != '')
        {
            $filtro = " AND i_menu = {$i_menu}";
        }

        $sql = "SELECT i_permissao
   					, tipo
                    , (SELECT nome FROM grupos AS z WHERE z.i_empresa = a.i_empresa AND z.i_grupo = a.i_grupo) AS nome_grupo
                    , (SELECT nome FROM usuarios AS z WHERE z.i_empresa = a.i_empresa AND z.i_usuario = a.i_usuario_fixo) AS nome_usuario
   					, libera_usuario_fixo
                FROM portal_permissoes AS a
                WHERE i_empresa={$this->session->userdata('i_empresa')}
                AND i_modulo = {$i_modulo}
                {$filtro} 
                AND NOT EXISTS(SELECT nome FROM usuarios AS z WHERE z.i_empresa = a.i_empresa AND z.i_usuario = a.i_usuario_fixo AND z.admin = 1)
                ORDER BY i_permissao ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }
}
