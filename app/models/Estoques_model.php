<?php

class Estoques_model extends Crud_model
{
    public $tabela = 'estoques';

    public $chave = array('i_empresa', 'i_estoque');

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
    	$sql = "SELECT i_produto
    				, codigo
    				, nome
    				, getEstoqueQtde(i_empresa, i_produto) AS qtde
    				, getEstoqueValor(i_empresa, i_produto) AS valor
    			FROM produtos
    			WHERE i_empresa = {$this->session->userdata('i_empresa')}
    			AND status = 'A'
    			ORDER BY nome ASC";
    	$query = $this->db->query($sql);
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }
}
