<?php

class Empresas_model extends Crud_model
{
    public $tabela = 'empresas';

    public $chave = array('i_empresa');

    public function __construct()
    {
        parent::__construct();
    }
}