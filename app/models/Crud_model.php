<?php

class Crud_model extends CI_Model
{
    public function salvar($data = array())
    {
        if (empty($data))
        {
            $data = $this->input->post();
        }
        $novo = 0;
        $filtro = array();
        foreach ($this->chave as $a) {
            if (trim($data[$a]) == '') {
                $novo = 1;
                break;
            }
            $filtro[$a] = $data[$a];
        }

        if ($novo === 1) {
            $this->db->insert($this->tabela, $data);
        } else {
            $this->db->update($this->tabela, $data, $filtro);
        }
    }

    public function get_id($filtro = '')
    {
        $query = $this->db->get_where($this->tabela, $filtro);
        return $query->row_array();
    }

    public function deletar($data = array())
    {
        $this->db->delete($this->tabela, $data);
    }
}
