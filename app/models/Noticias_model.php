<?php

class Noticias_model extends Crud_model
{
    public $tabela = 'noticias';

    public $chave = array('i_empresa', 'i_noticia');

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        $this->db->select('i_noticia, titulo');
        $this->db->from('noticias');
        $this->db->where('i_empresa', $this->session->userdata('i_empresa'));
        $this->db->order_by('dt_cadastro', 'DESC');
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }
}
