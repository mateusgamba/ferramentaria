<?php

class Grupos_model extends Crud_model
{
    public $tabela = 'grupos';

    public $chave = array('i_empresa', 'i_grupo');

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        $this->db->select('i_grupo, nome');
        $this->db->from('grupos');
        $this->db->where('i_empresa', $this->session->userdata('i_empresa'));
        $this->db->order_by('nome', 'ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    public function get_permissoes($data_subquery = array())
    {
        $this->db->select('i_grupo, nome');
        $this->db->from('grupos');
        $this->db->where('i_empresa', $this->session->userdata('i_empresa'));
        $sub = $this->subquery->start_subquery('where_exists');
        $sub->select('1')->from('portal_permissoes');
        $sub->where('portal_permissoes.i_empresa = grupos.i_empresa');
        $sub->where('portal_permissoes.i_grupo = grupos.i_grupo');
        $sub->where($data_subquery);
        $this->subquery->end_subquery('', FALSE);
        $this->db->order_by('nome', 'ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
        return $result;

        $result = $query->result_array();
        $query->free_result();
        return $result;
    }
}
