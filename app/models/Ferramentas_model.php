<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ferramentas_model extends CI_model
{
	public function get_ferramentas()
	{
		$this->db->select('i_ferramenta, descricao');
		$this->db->from('ferramentas');
		$this->db->where('i_empresa', 1);
		$this->db->order_by('descricao', 'ASC');
		$query = $this->db->get();
		$result = $query->result_array();
		$query->free_result();
		return $result;
	}

	public function salvar($data)
	{
		if (trim($data['i_ferramenta'])=='' ) {
			$this->db->insert('ferramentas', $data);
		} else {
			$data_filtro = array(
							'i_empresa' => $data['i_empresa'],
							'i_ferramenta' => $data['i_ferramenta']
							);
			$this->db->update('ferramentas', $data, $data_filtro);
		}
	}

	public function get($data_filtro)
	{
		$query = $this->db->get_where('ferramentas', $data_filtro);
		$obj = $query->row_array();
		$query->free_result();
		return $obj;
	}
}
