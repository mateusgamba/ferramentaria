<?php

function link_script($src = '')
{
    if ($src != '') {
        $CI =& get_instance();
        $base_principal = $CI->config->slash_item('base_url');
        return "<script src=\"{$base_principal}{$src}\"></script>";
    }
    return '';
}

function get_menus($i_usuario)
{
    $ci =& get_instance();
    $ci->load->database();
    $ci->load->Model('Portal_modulos_model');
    return $ci->Portal_modulos_model->get_modulos_menus_topo();
}
