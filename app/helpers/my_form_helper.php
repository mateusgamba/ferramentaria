<?php

function my_input($tamanho = '', $nome_label = '', $nome_input = '', $value_input = '', $maxlength_input = '', $valida = 0, $classes = '', $outros = NULL)
{
	$input_attributes = array(
							'name' => $nome_input,
							'id' => $nome_input,
							'value' => $value_input,
							'maxlength' => $maxlength_input,
							'class' => "form-control input-sm {$classes}"
						);

	if ($valida === 1) {
		$input_attributes = array_merge($input_attributes, array('valida' => 1));
	}

	if (is_array($outros)) {
		$input_attributes = array_merge($input_attributes,$outros);
	}
	echo "<div class=\"col-{$tamanho} col-sm-{$tamanho} col-lg-{$tamanho}\">";
	if ($nome_label != '') {
		echo form_label($nome_label, $nome_input, array('class' => 'control-label'));
	}
	echo form_input($input_attributes);
	echo '</div>';
}

/*

function my_form_open($nome_form = '', $caminho_salvar = '', $caminho_retorno = '', $tipo = '', $outros = NULL, $msg = 1, $class = '')
{
	$CI =& get_instance();
	$base = $CI->config->slash_item('base_url').$CI->config->slash_item('index_page');

	$attributes = array(
					'name'=>$nome_form,
					'id'=>$nome_form,
					'method'=>'POST',
					'action'=>"",
					'onsubmit'=>"return salvar_formulario(this, '{$base}{$caminho_salvar}', '{$base}{$caminho_retorno}', {$msg});"
				  );
	if (is_array($outros))
	{
		$attributes = array_merge($attributes,$outros);
	}
	echo "<form class='{$class}' name='$nome_form' id='$nome_form' method='POST' onsubmit='return salvar_formulario(this, \"{$base}{$caminho_salvar}\", \"{$base}{$caminho_retorno}\", {$msg})'>";
}

function my_form_close()
{
	echo form_close();
}


function my_dropdown($tamanho = "", $nome_label = "", $nome_input = "",  $options = "", $selected = NULL, $valida = FALSE, $classes = "", $attributes = "")
{
	$str = " id='{$nome_input}' valida='{$valida}' class='form-control input-sm {$classes}'";
	if ($attributes)
	{
		foreach($attributes as $key => $val)
		{
			$str .= " {$key}='{$attributes[$key]}'";
		}
	}

	echo "<div class='col-{$tamanho} col-sm-{$tamanho} col-lg-{$tamanho} {$classes}'>";
	echo form_label($nome_label, $nome_input, array('class'=>'control-label'));
	echo form_dropdown($nome_input, $options, $selected, $str);
	echo "</div>";
}

function my_checkbox($tamanho = "", $nome_label = "", $name_input = "", $id_input = "", $value_input = '', $checked = NULL, $classes = "", $outros = NULL)
{
	$input_attributes = array(
		'name' => $name_input,
		'id' => $id_input,
		'value' => $value_input,
		'checked' => $checked
	);

	if (is_array($outros))
	{
		$input_attributes = array_merge($input_attributes,$outros);
	}

	echo "<div class='col-{$tamanho} col-sm-{$tamanho} col-lg-{$tamanho} {$classes}'>";
	echo "<label class='control-label'>&nbsp;</label>";
	echo "<div class='checkbox'>";
	echo "<label>".form_checkbox($input_attributes)." {$nome_label}</label>";
	echo "</div>";
	echo "</div>";
}

function my_input_botao($tamanho = "", $nome_label = "", $nome_input = "", $value_input = "", $maxlength_input = "", $valida = FALSE, $link = "", $classes = "", $outros = NULL)
{
	$CI =& get_instance();
	$base_principal = $CI->config->slash_item('base_url');
	$index_page = $CI->config->slash_item('index_page');

	$input_attributes = array('name' => $nome_input,
							  'id' => $nome_input,
							  'value' => $value_input,
							  'maxlength' => $maxlength_input,
							  'class' => "form-control input-sm {$classes}"
						);

	if ($valida==TRUE)
	{
		$input_attributes = array_merge($input_attributes, array("valida"=>"true"));	
	}

	if (is_array($outros))
	{
		$input_attributes = array_merge($input_attributes,$outros);
	}
	echo "<div class='col-{$tamanho} col-sm-{$tamanho} col-lg-{$tamanho}'>";
	if ($nome_label!="")
	{
		echo form_label($nome_label, $nome_input, array('class'=>'control-label'));
	}
	echo "<div class='input-group'>";
	echo form_input($input_attributes);
	echo "<a href='javascript:void(0)'
			 	class='btn btn-sm input-group-addon glyphicon glyphicon-search gabox2'
				data-remote='".$base_principal.$index_page."/{$link}'
				data-title='Consulta Fornecedor'
				data-target='buscar_fornecedor'></a>";
	echo "</div>";
	echo "</div>";
}

function my_input_h($tamanho = "", $nome_label = "", $nome_input = "", $value_input = "", $maxlength_input = "", $valida = FALSE, $classes = "", $outros = NULL)
{
	$tamanho_explode = explode(",", $tamanho);
	$tamanho_label = $tamanho_explode[0];
	$tamanho_input = $tamanho_explode[1];

	$input_attributes = array('name' => $nome_input,
							  'id' => $nome_input,
							  'value' => $value_input,
							  'maxlength' => $maxlength_input,
							  'class' => "form-control input-sm {$classes}"
						);

	if ($valida==TRUE)
	{
		$input_attributes = array_merge($input_attributes, array("valida"=>"true"));	
	}

	if (is_array($outros))
	{
		$input_attributes = array_merge($input_attributes,$outros);
	}

  	echo "<div class='form-group'>";
  	if ($nome_label!="")
  	{
    	echo "<label for='{$nome_input}' class='col-{$tamanho_label} col-sm-{$tamanho_label} col-lg-{$tamanho_label} control-label'>{$nome_label}</label>";
    }
    echo "<div class='col-{$tamanho_input} col-sm-{$tamanho_input} col-lg-{$tamanho_input}'>";
	echo form_input($input_attributes);
	echo "</div>";
	echo "</div>";	
}

function my_textarea($tamanho = "", $nome_label = "", $nome_input = "", $value_input = "", $maxlength_input = "", $valida = FALSE, $classes = "", $outros = NULL)
{
	$input_attributes = array('name' => $nome_input,
							  'id' => $nome_input,
							  'value' => $value_input,
							  'maxlength' => $maxlength_input,
							  'class' => "form-control input-sm {$classes}",
							  'rows'=>'3'
						);

	if ($valida==TRUE)
	{
		$input_attributes = array_merge($input_attributes, array("valida"=>"true"));	
	}

	if (is_array($outros))
	{
		$input_attributes = array_merge($input_attributes,$outros);
	}
	echo "<div class='col-{$tamanho} col-sm-{$tamanho} col-lg-{$tamanho}'>";
	if ($nome_label!="")
	{
		echo form_label($nome_label, $nome_input, array('class'=>'control-label'));
	}
	echo form_textarea($input_attributes);
	echo "</div>";
}

 //Novos Componentes 

function my_campo($tamanho = "", $nome_label = "&nbsp;", $nome_input = "", $value_input = "", $valida = FALSE, $maxlength_input = 100, $classes = "", $outros = "")
{
	$input_attributes = array('name' => $nome_input,
							  'id' => $nome_input,
							  'value' => $value_input,
							  'maxlength' => $maxlength_input,
							  'class' => "form-control input-sm {$classes}",
							  'valida' => $valida
						);
	if (is_array($outros))
	{
		$input_attributes = array_merge($input_attributes,$outros);
	}

	echo "<div class='col-sm-{$tamanho}'>";
	echo "<div class='form-group'>";
	echo form_label($nome_label, $nome_input);
	echo form_input($input_attributes);
	echo "</div>";
	echo "</div>";
}

function my_campo_codigo($tamanho = "", $nome_label = "&nbsp;", $nome_input = "", $value_input = "", $valida = FALSE, $maxlength_input = 100, $classes = "", $outros = "", $titulo_modal = "", $remote = "", $remote_attributes = "", $classes_botoes = "")
{
	$input_attributes = array('name' => $nome_input,
							  'id' => $nome_input,
							  'value' => $value_input,
							  'maxlength' => $maxlength_input,
							  'class' => "form-control input-sm {$classes}",
							  'valida' => $valida
						);
	if (is_array($outros))
	{
		$input_attributes = array_merge($input_attributes,$outros);
	}

	$query_string = "";

	if ($remote_attributes)
	{
		foreach($remote_attributes as $key=>$val)
		{
			if ($query_string=="")
			{
				$query_string = "?{$key}={$val}";
			}
			else
			{
				$query_string .= "&{$key}={$val}";
			}
		}
	}

	$CI =& get_instance();
	$base_principal = $CI->config->slash_item('base_url');
	$index_page = $CI->config->slash_item('index_page');

	$button_attributes = array('type'=>"button",
							   'id'=>"btn_{$nome_input}",
							   'class'=>"btn btn-default btn-sm gabox21 {$classes_botoes}",
							   'data-target'=>"modal_{$nome_input}",
    						   'data-title'=>$titulo_modal,
    						   'content'=>"<span class='glyphicon icon-search'></span>"
	);

	if(strpos($remote, "javascript") !== false)
	{
		$button_attributes = array_merge($button_attributes, array('onclick'=>$remote));
	}
	else
	{
		$button_attributes = array_merge($button_attributes, array('data-remote'=>$base_principal.$index_page."/".$remote.$query_string."&id_box=modal_{$nome_input}"));
	}

	echo "<div class='col-sm-{$tamanho}'>";
	echo "<div class='form-group'>";
	echo form_label($nome_label, $nome_input);
	echo "<div class='input-group'>";
	echo form_input($input_attributes);						
	echo "<span class='input-group-btn'>";
	echo form_button($button_attributes);
	echo "</span>";
	echo "</div>";
	echo "</div>";
	echo "</div>";	
}

function my_campo_data($tamanho = "", $nome_label = "&nbsp;", $nome_input = "", $value_input = "", $valida = FALSE, $maxlength_input = 10, $classes = "", $outros = "")
{
	$input_attributes = array('name' => $nome_input,
							  'id' => $nome_input,
							  'value' => $value_input,
							  'maxlength' => $maxlength_input,
							  'class' => "form-control input-sm mask_date {$classes}",
							  'valida' => $valida
						);
	if (is_array($outros))
	{
		$input_attributes = array_merge($input_attributes,$outros);
	}

	echo "<div class='col-sm-{$tamanho}'>";
	echo "<div class='form-group'>";
	echo form_label($nome_label, $nome_input);
	echo "<div data-date-autoclose='true' data-date-format='dd/mm/yyyy' class='input-group date ebro_datepicker'>";
	echo form_input($input_attributes);
	echo "<span class='input-group-addon icon_addon'><span class='glyphicon icon-calendar'></span></span>";
	echo "</div>";
	echo "</div>";
	echo "</div>";
}

function my_campo_time($tamanho = "", $nome_label = "&nbsp;", $nome_input = "", $value_input = "", $valida = FALSE, $maxlength_input = 10, $classes = "", $outros = "")
{
	$input_attributes = array('name' => $nome_input,
							  'id' => $nome_input,
							  'value' => $value_input,
							  'maxlength' => $maxlength_input,
							  'class' => "form-control input-sm {$classes}",
							  'valida' => $valida
						);
	if (is_array($outros))
	{
		$input_attributes = array_merge($input_attributes,$outros);
	}
	echo "<div class='col-sm-{$tamanho}'>";
    echo form_label($nome_label, $nome_input);
    echo "<div class='input-group bootstrap-timepicker'>";
    echo form_input($input_attributes);           
    echo "<span class='input-group-addon'><i class='icon-time'></i></span>";
    echo "</div>";
    echo "</div>";
}

function my_combobox($tamanho = "", $nome_label = "&nbsp;", $nome_input = "",  $options = "", $selected = NULL, $valida = FALSE, $classes = "", $outros = "")
{
	$str = " id='{$nome_input}' valida='{$valida}' class='form-control input-sm {$classes}'";
	if ($outros)
	{
		foreach($outros as $key => $val)
		{
			$str .= " {$key}='{$val}'";
		}
	}

	echo "<div class='col-sm-{$tamanho}'>";
	echo "<div class='form-group'>";
	echo form_label($nome_label, $nome_input);
	echo form_dropdown($nome_input, $options, $selected, $str);
	echo "</div>";
	echo "</div>";
}

function my_texto($tamanho = "", $nome_label = "&nbsp;", $nome_input = "", $value_input = "", $valida = FALSE, $maxlength = 0, $rows = 2, $cols = 10, $classes = "", $outros = "")
{
	$attributes = array('name' => $nome_input,
						'id' => $nome_input,
						'value' => $value_input,
						'class' => "form-control {$classes}",
						'rows'=>$rows,
						'cols'=>$cols,
						'valida' => $valida
						);
	
	if ($maxlength>0)
	{
		$attributes = array_merge($attributes, array("maxlength"=>$maxlength));	
	}

	if (is_array($outros))
	{
		$attributes = array_merge($attributes,$outros);
	}

	echo "<div class='col-sm-{$tamanho}'>";
	echo "<div class='form-group'>";
	echo form_label($nome_label, $nome_input);
	echo form_textarea($attributes);
	echo "</div>";
	echo "</div>";
}

function my_hidden($nome_input, $valor)
{
	echo "<input type='hidden' name='{$nome_input}' id='{$nome_input}' value='{$valor}'>";
}

function my_botao($nome_botao = "", $tipo_botao = "", $content = "", $onclick = "", $outros = "", $classes = "")
{
	$attributes = array(
		'name'=>$nome_botao,
    	'id'=>$nome_botao,
	    'type'=>$tipo_botao,
    	'content'=>$content,
    	'class'=>"btn btn-default btn-sm {$classes}");

	if ($onclick!="")
	{
		$attributes = array_merge($attributes, array("onclick"=>$onclick));
	}

	if ($outros!="")
	{
		$attributes = array_merge($attributes, $outros);
	}	

	echo form_button($attributes);
}

function my_botao_popup($nome_botao = "", $tipo_botao = "", $content = "", $onclick = "", $width = "600", $height="400", $outros = "", $classes = "")
{
	$attributes = array(
		'name'=>$nome_botao,
    	'id'=>$nome_botao,
	    'type'=>$tipo_botao,
    	'content'=>$content,
    	'class'=>"btn btn-default btn-sm {$classes}");

	if ($onclick!="")
	{
		$attributes = array_merge($attributes, array("onclick"=>" window.open('".site_url($onclick)."', '{$nome_botao}', 'width={$width}, height={$height}, scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');"));
	}

	if ($outros!="")
	{
		$attributes = array_merge($attributes, $outros);
	}	

	echo form_button($attributes);
}

function my_botao_relatorio($nome_botao = "", $content = "", $onclick = "", $outros = "", $classes = "")
{
	$attributes = array(
		'name'=>$nome_botao,
		'id'=>$nome_botao,
		'type'=>'button',
		'content'=>$content,
		'class'=>"btn btn-default btn-sm {$classes}");

	if ($onclick!="")
	{
		$attributes = array_merge($attributes, array("onclick"=>"window.open('".base_url().index_page()."/relatorios/abre_relatorio?url=".$onclick."','_blank','height='+screen.availHeight+',width='+screen.width+',top=0,left=0,status=yes,scrollbars=yes,resizable=yes')"));
	}

	if ($outros!="")
	{
		$attributes = array_merge($attributes, $outros);
	}		
	
	echo form_button($attributes);
}

function my_check($tamanho = "", $nome_label = "&nbsp;", $nome_input = "", $value_input = "", $checked = FALSE, $valida = FALSE, $classes = "", $outros = "")
{
	$input_attributes = array('name' => $nome_input,
							  'id' => $nome_input,
							  'value' => $value_input,
							  'class' => "input-sm {$classes}",
							  'valida' => $valida,
							  'checked' => $checked
						);
	if (is_array($outros))
	{
		$input_attributes = array_merge($input_attributes,$outros);
	}

	echo "<div class='col-sm-{$tamanho}'>";
	//echo "<div class='form-group'>";
	echo "<div class='checkbox'>";

	echo form_label($nome_label.form_checkbox($input_attributes), $nome_input);

	echo "</div>";
	echo "</div>";
}
*/

/* End of file my_form_helper.php */
/* Location: ./app/helpers/my_form_helper.php */