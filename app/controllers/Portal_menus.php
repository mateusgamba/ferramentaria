<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal_menus extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Portal_menus_model');
	}

	public function index($i_modulo)
	{
		$data['lista_menus'] = $this->Portal_menus_model->get_menus($i_modulo);
		$data['i_modulo'] = $i_modulo;
		$this->template->load('portal_menus/index', $data);
 	}

	public function adicionar($i_modulo)
	{
		$data['i_modulo'] = $i_modulo;
		$this->template->load('portal_menus/adicionar', $data);
 	}

	public function salvar()
	{
		$this->Portal_menus_model->salvar();
 		redirect('portal_menus/index/'.$this->input->post('i_modulo'));
	}

	public function alterar($i_modulo = '', $i_menu = '')
	{
		$filtro = array(
					'i_empresa' => $this->session->userdata('i_empresa'),
					'i_modulo' => $i_modulo,
					'i_menu' => $i_menu
				);
		$data = $this->Portal_menus_model->get_id($filtro);
		$data['i_modulo'] = $i_modulo;
		$this->template->load('portal_menus/adicionar', $data);
	}
}
