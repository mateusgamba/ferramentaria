<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupos extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Grupos_model', 'My_controller_model');
	}

	public function index()
	{
		$data['lista'] = $this->My_controller_model->get_all();
		$this->template->load(strtolower(__CLASS__).'/'.__FUNCTION__ , $data);
	}

	public function adicionar()
	{
		$this->template->load(strtolower(__CLASS__).'/'.__FUNCTION__);
	}

	public function salvar()
	{
		$this->My_controller_model->salvar();
 		redirect('grupos/index');
	}

	public function alterar($i_grupo = '')
	{
		$filtro = array(
					'i_empresa' => $this->session->userdata('i_empresa'),
					'i_grupo' => $i_grupo
				);
		$data = $this->My_controller_model->get_id($filtro);
		$this->template->load('grupos/adicionar', $data);
	}
}
