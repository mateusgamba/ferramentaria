<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ferramentas extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ferramentas_model');
	}

	public function index()
	{
		$data['lista_ferramentas'] = $this->Ferramentas_model->get_ferramentas();
		$this->template->load('ferramentas/index', $data);
	}

	public function adicionar()
	{
		$this->template->load('ferramentas/adicionar');
	}

	public function salvar()
	{
		$data = $this->input->post();
		$this->Ferramentas_model->salvar($data);
		redirect('ferramentas/index');
	}

	public function alterar($id = '')
	{
		$data = $this->Ferramentas_model->get(array('i_empresa'=>1, 'i_ferramenta'=>$id));
		$this->template->load('ferramentas/adicionar', $data);
	}
}
