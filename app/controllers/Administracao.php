<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administracao extends CI_Controller
{
	public function index()
	{
		$this->template->load('administracao/index');
	}
}