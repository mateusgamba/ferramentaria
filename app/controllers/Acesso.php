<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acesso extends CI_Controller
{
    public function index()
    {
        $this->load->view('acesso/index');
    }

    public function acessar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required|callback__is_autenticar');
        if ($this->form_validation->run() === FALSE) {
            $this->index();
        } else {
            redirect('administracao');
        }
    }

    public function _is_autenticar($senha)
    {
        $email = $this->input->post('email');
        $obj = $this->Usuarios_model->get_usuario($email, $senha);
        if (!empty($obj)) {
            $array_usuario = array(
                'i_empresa' => 1,
                'i_usuario' => $obj['i_usuario'],
                'nome' => $obj['nome'],
                'email' => $email,
                'logado' => 1
            );
            $this->session->set_userdata($array_usuario);
            return TRUE;
        } else {
            $this->form_validation->set_message('_is_autenticar', 'Seu E-mail ou Senha está incorretos');
            return FALSE;
        }
    }

    public function sair()
    {
        $this->session->sess_destroy();
        redirect('acesso/index');
    }
}
