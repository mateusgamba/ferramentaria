<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Noticias_model', 'My_controller_model');
    }

    public function index()
    {
        $data['lista'] = $this->My_controller_model->get_all();
        $this->template->load(strtolower(__CLASS__).'/'.__FUNCTION__, $data);
    }

    public function adicionar()
    {
        $this->template->load(strtolower(__CLASS__).'/'.__FUNCTION__);
    }

    public function salvar()
    {
        $this->My_controller_model->salvar();
        redirect(strtolower(__CLASS__).'/index');
    }

    public function alterar($i_noticia = '')
    {
        $filtro = array(
            'i_empresa' => $this->session->userdata('i_empresa'),
            'i_noticia' => $i_noticia
        );
        $data = $this->My_controller_model->get_id($filtro);
        $this->template->load(strtolower(__CLASS__).'/adicionar', $data);
    }
}
