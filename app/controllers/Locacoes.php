<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Locacoes extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Locacoes_model');
    }

    public function index()
    {
        $data['lista_locacoes'] = $this->Locacoes_model->get_locacoes();
        $this->template->load('locacoes/index', $data);
    }

    public function adicionar()
    {
        $this->template->load('locacoes/adicionar');
    }

    public function busca_usuarios()
    {
        $dados = array();
        $valor = $this->input->get('q[term]');
        $lista_usuarios = $this->Usuarios_model->getUsuariosFiltro($valor);
        foreach ($lista_usuarios as $mostra) {
            $dados[] = array(
                        'id' => $mostra['i_usuario'],
                        'text' => $mostra['nome']
            );
        }
        $data = array(
            'total' => count($dados),
            'results' => $dados
        );
        echo json_encode($data);
    }
}
