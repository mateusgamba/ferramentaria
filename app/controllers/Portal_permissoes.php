<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal_permissoes extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Portal_modulos_model');
        $this->load->model('Grupos_model');
        $this->load->model('Usuarios_model');
        $this->load->model('Portal_permissoes_model');
    }

    public function index($i_modulo = '', $i_menu = '')
    {
        $data['lista_menus'] = $this->Portal_modulos_model->get_modulos_menus_topo();

        $data['input_menu'] = array();

        if (!empty($i_modulo)) {
            $filtro = array(
                'i_empresa' => $this->session->userdata('i_empresa'),
                'i_modulo' => $i_modulo
            );
            $filtro_permissoes = array(
                    'i_modulo' => (int)$i_modulo,
                    'i_menu' => NULL
            );
            $data['obj_modulo'] = $this->Portal_modulos_model->get_id($filtro);
        }
        
        if (!empty($i_menu)) {
            $filtro = array(
                'i_empresa' => (int)$this->session->userdata('i_empresa'),
                'i_modulo' => (int)$i_modulo,
                'i_menu' => (int)$i_menu
            );
            $filtro_permissoes = array(
                    'i_modulo' => (int)$i_modulo,
                    'i_menu' => (int)$i_menu
            );            
            $data['obj_menu'] = $this->Portal_menus_model->get_id($filtro);
            $data['input_menu'] = array('i_menu' => $i_menu);
        }

        if ((!empty($i_modulo)) || (!empty($i_menu))) {
            $data['lista_grupos'] = $this->Grupos_model->get_permissoes($filtro_permissoes);
            $data['lista_usuario'] = $this->Usuarios_model->get_permissoes($filtro_permissoes);
            $data['lista_permissoes'] = $this->Portal_permissoes_model->get_permissoes_menu($i_modulo, $i_menu);
        }

        $data['i_menu'] = trim($i_menu)!=''?$i_menu:'NULL';
        $data['i_modulo'] = $i_modulo;

        $this->template->load('portal_permissoes/index', $data);
    }

    public function salvar()
    {
        $this->Portal_permissoes_model->salvar();
        $i_modulo = $this->input->post('i_modulo');
        $i_menu = $this->input->post('i_menu');
        redirect('portal_permissoes/index/'.$i_modulo.'/'.$i_menu);
    }

    public function deletar($i_modulo, $i_menu, $i_permissao)
    {
        $data = array(
            'i_empresa' => $this->session->userdata('i_empresa'),
            'i_permissao' => $i_permissao
        );
        $this->Portal_permissoes_model->deletar($data);
        redirect('portal_permissoes/index/'.$i_modulo.'/'.$i_menu);
    }
}
