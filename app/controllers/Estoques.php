<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estoques extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Estoques_model', 'My_controller_model');
	}

	public function index()
	{
		$data['lista'] = json_encode($this->My_controller_model->get_all());
		$this->template->load(strtolower(__CLASS__).'/'.__FUNCTION__ , $data);
	}

	public function salvar()
	{
		$this->My_controller_model->salvar();
 		redirect('estoques/index');
	}
}
