<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function index()
	{
		$data['lista_usuarios'] = $this->Usuarios_model->get_usuarios();
		$this->template->load('usuarios/index', $data);
	}

	public function adicionar()
	{
		$this->template->load('usuarios/adicionar');
	}

	public function salvar()
	{
		$this->Usuarios_model->salvar();
 		redirect('usuarios/index');
	}

	public function alterar($i_usuario)
	{
		$filtro = array(
					'i_empresa' => $this->session->userdata('i_empresa'),
					'i_usuario' => $i_usuario
				);
		$data = $this->Usuarios_model->get_id($filtro);
		$this->template->load('usuarios/adicionar', $data);
	}
}
