<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Empresas_model', 'My_controller_model');
    }

    public function index()
    {
        $filtro = array(
            'i_empresa' => $this->session->userdata('i_empresa')
        );
        $data = $this->My_controller_model->get_id($filtro);
        $this->template->load('empresas/index', $data);
    }

    public function salvar()
    {
        $this->My_controller_model->salvar();
        redirect('empresas/index');
    }
}