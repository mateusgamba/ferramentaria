<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal_modulos extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Portal_modulos_model');
	}

	public function index()
	{
		$data['lista_modulos'] = $this->Portal_modulos_model->get_modulos();
		$this->template->load('portal_modulos/index', $data);
	}

	public function adicionar()
	{
		$this->template->load('portal_modulos/adicionar');
	}

	public function salvar()
	{
		$this->Portal_modulos_model->salvar();
 		redirect('portal_modulos/index');
	}

	public function alterar($i_modulo = '')
	{
		$filtro = array(
					'i_empresa' => $this->session->userdata('i_empresa'),
					'i_modulo' => $i_modulo
				);
		$data = $this->Portal_modulos_model->get_id($filtro);
		$this->template->load('portal_modulos/adicionar', $data);
	}
}
