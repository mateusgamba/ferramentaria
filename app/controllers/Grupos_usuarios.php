<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupos_usuarios extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Grupos_usuarios_model');
	}

	public function index($i_grupo)
	{
		$data['lista'] = $this->Grupos_usuarios_model->get_usuarios_permissoes($i_grupo);
		$data['i_grupo'] = $i_grupo;
		$this->template->load('grupos_usuarios/index', $data);
 	}

 	public function salvar()
 	{
 		$data['i_empresa'] = $this->input->post('i_empresa');
 		$data['i_grupo'] = $this->input->post('i_grupo');
 		$usuarios = $this->input->post('usuarios');
 		
 		$this->Grupos_usuarios_model->delete($data);
 		
 		foreach ($usuarios as $a)
 		{
			$data['i_usuario'] = $a;
 			$this->Grupos_usuarios_model->salvar($data);
 		}
 		redirect('grupos_usuarios/index/'.$data['i_grupo']);
 	}
}
