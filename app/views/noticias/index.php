<div class="page-header">
    <h2>Notícias</h2>
</div>

<p>
    <?php echo anchor('noticias/adicionar', 'Adicionar', array('class' => 'btn btn-default btn-sm')); ?>
</p>

<div class="panel panel-default">
    <div class="panel-body">
        <?php
            if (count($lista)>0) {
        ?>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th width="10%" class="text-center">Código</th>
                    <th>Título</th>
                    <th width="10%" class="text-center">Alterar</th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($lista as $a) {
                echo '<tr>';
                echo "<td class=\"text-center\">{$a['i_noticia']}</td>";
                echo "<td>{$a['titulo']}</td>";
                echo '<td class="text-center">';
                echo anchor('noticias/alterar/'.$a['i_noticia'], '<span class="glyphicon glyphicon-pencil"></span>', array('class'=>'btn btn-default btn-xs', 'role'=>'button'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
        <?php
            } else {
                echo '<br>';
                echo '<div class="alert alert-info" role="alert">';
                echo 'No momento não possui nenhuma notícia adicionada, '.anchor('noticias/adicionar', 'Clique aqui para Adicionar').'</a>';
                echo '</div>';
            }
        ?>
    </div>
</div>