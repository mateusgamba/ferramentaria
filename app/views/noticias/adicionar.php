<div class="page-header">
    <h2>Cadastrar Notícias</h2>
</div>

<p>
    <?php echo anchor('noticias/index', 'Voltar', array('class' => 'btn btn-default btn-sm')); ?>
</p>

<div class="panel panel-default">
    <div class="panel-body">

        <?php echo form_open('noticias/salvar', '', array('i_empresa' => $this->session->userdata('i_empresa'))); ?>

            <div class="row">
                <div class="form-group col-md-1">
                    <label for="i_noticia" class="control-label">Código:</label>
                    <input type="input" class="form-control" id="i_noticia" name="i_noticia" value="<?php echo isset($i_noticia)?$i_noticia:''; ?>" readonly>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="titulo" class="control-label">Título:</label>
                    <input type="input" class="form-control" id="titulo" name="titulo" value="<?php echo isset($titulo)?$titulo:''; ?>" required autofocus>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='A' name='status'>
                            <input type="checkbox" id="status" name="status" value="I" <?php echo isset($status) && $status === 'I'?'checked':''; ?>> Inativar
                        </label>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-default">Salvar</button>

        <?php echo form_close(); ?>

    </div>
</div>
        