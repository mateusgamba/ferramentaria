<div class="page-header">
    <h2>Grupos</h2>
</div>

<p>
	<?php echo anchor('grupos/adicionar', 'Adicionar', array('class' => 'btn btn-default btn-sm')); ?>
</p>

<div class="panel panel-default">
    <div class="panel-body">
    	<?php
    		if (count($lista)>0)
    		{
    	?>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th width="10%" class="text-center">Código</th>
					<th>Descrição</th>
					<th width="10%" class="text-center">Usuários</th>
					<th width="10%" class="text-center">Alterar</th>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach ($lista as $a)
				{
					echo '<tr>';
					echo "<td class=\"text-center\">{$a['i_grupo']}</td>";
					echo "<td>{$a['nome']}</td>";
					echo '<td class="text-center">';
					echo anchor('grupos_usuarios/index/'.$a['i_grupo'], '<span class="glyphicon glyphicon-user"></span>', array('class'=>'btn btn-default btn-xs', 'role'=>'button'));
					echo '</td>';
					echo '<td class="text-center">';
					echo anchor('grupos/alterar/'.$a['i_grupo'], '<span class="glyphicon glyphicon-pencil"></span>', array('class'=>'btn btn-default btn-xs', 'role'=>'button'));
					echo '</td>';				
					echo '</tr>';
				}
			?>
			</tbody>
		</table>
		<?php
			} else {
				echo '<br>';
				echo '<div class="alert alert-info" role="alert">';
				echo 'No momento não possui nenhum menu adicionado, '.anchor('grupos/adicionar', 'Clique aqui para Adicionar').'</a>';
				echo '</div>';
			}
		?>
    </div>
</div>