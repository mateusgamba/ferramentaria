<div class="page-header">
    <h2>Cadastrar Grupo</h2>
</div>

<p>
    <?php echo anchor('grupos/index', 'Voltar', array('class' => 'btn btn-default btn-sm')); ?>
</p>

<div class="panel panel-default">
    <div class="panel-body">

        <?php echo form_open('grupos/salvar', '', array('i_empresa' => $this->session->userdata('i_empresa'), 'i_usuario' => $this->session->userdata('i_usuario'))); ?>

            <div class="row">
                <div class="form-group col-md-1">
                    <label for="i_grupo" class="control-label">Código:</label>
                    <input type="input" class="form-control" id="i_grupo" name="i_grupo" value="<?php echo isset($i_grupo)?$i_grupo:''; ?>" readonly>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="nome" class="control-label">Nome:</label>
                    <input type="input" class="form-control" id="nome" name="nome" value="<?php echo isset($nome)?$nome:''; ?>" required autofocus>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='A' name='status'>
                            <input type="checkbox" id="status" name="status" value="I" <?php echo isset($status) && $status === 'I'?'checked':''; ?>> Inativar
                        </label>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-default">Salvar</button>

        <?php echo form_close(); ?>

    </div>
</div>
        