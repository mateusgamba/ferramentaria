<div class="page-header">
    <h2>Ferramentas</h2>
</div>

<p>
	<?php echo anchor('ferramentas/adicionar', 'Adicionar', array('class' => 'btn btn-default btn-sm')); ?>
</p>

<div class="panel panel-default">
    <div class="panel-body">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="text-center" width="10%">Código</th>
					<th>Descrição</th>
					<th class="text-center" width="10%">Alterar</th>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach ($lista_ferramentas as $a)
				{
					echo '<tr>';
					echo "<td class=\"text-center\">{$a['i_ferramenta']}</td>";
					echo "<td>{$a['descricao']}</td>";
					echo '<td class="text-center">';
					echo anchor('ferramentas/alterar/'.$a['i_ferramenta'], '<span class="glyphicon glyphicon-pencil"></span>', array('class'=>'btn btn-default btn-xs', 'role'=>'button'));
					echo '</tr>';
				}
			?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3"><strong>Total: <?php echo count($lista_ferramentas); ?></strong></td>
				</tr>
			</tfoot>
		</table>
    </div>
</div>