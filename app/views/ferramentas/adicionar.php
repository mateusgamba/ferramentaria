<div class="page-header">
    <h2>Cadastrar Ferramenta</h2>
</div>

<p>
    <?php echo anchor('ferramentas', 'Voltar', array('class' => 'btn btn-default btn-sm')); ?>
</p>

<div class="panel panel-default">
    <div class="panel-body">

        <?php echo form_open('ferramentas/salvar', '', array('i_empresa'=>1)); ?>

            <div class="row">
                <div class="form-group col-md-1">
                    <label for="i_ferramenta" class="control-label">Código:</label>
                    <input type="input" class="form-control" id="i_ferramenta" name="i_ferramenta" value="<?php echo isset($i_ferramenta)?$i_ferramenta:''; ?>" readonly>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="codigo_barras" class="control-label">Código de Barras:</label>
                    <input type="input" class="form-control" id="codigo_barras" name="codigo_barras" value="<?php echo isset($codigo_barras)?$codigo_barras:''; ?>">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="descricao" class="control-label">Descrição:</label>
                    <input type="input" class="form-control" id="descricao" name="descricao" value="<?php echo isset($descricao)?$descricao:''; ?>">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="fabricante" class="control-label">Fabricante:</label>
                    <input type="input" class="form-control" id="fabricante" name="fabricante" value="<?php echo isset($fabricante)?$fabricante:''; ?>">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="modelo" class="control-label">Modelo:</label>
                    <input type="input" class="form-control" id="modelo" name="modelo" value="<?php echo isset($modelo)?$modelo:''; ?>">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="numero_serie" class="control-label">Número de Série:</label>
                    <input type="input" class="form-control" id="numero_serie" name="numero_serie" value="<?php echo isset($numero_serie)?$numero_serie:''; ?>">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="patrimonio" class="control-label">Patrimônio:</label>
                    <input type="input" class="form-control" id="patrimonio" name="patrimonio" value="<?php echo isset($patrimonio)?$patrimonio:''; ?>">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='A' name='status'>
                            <input type="checkbox" id="status" name="status" value="I" <?php echo isset($status) && $status === 'I'?'checked':''; ?>> Inativar
                        </label>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-default">Salvar</button>

        <?php echo form_close(); ?>

    </div>
</div>
        