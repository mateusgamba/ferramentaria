<div class="page-header">
    <h2>Cadastrar Usuário</h2>
</div>

<p>
	<?php echo anchor('usuarios/adicionar', 'Adicionar', array('class' => 'btn btn-default btn-sm')); ?>
</p>

<div class="panel panel-default">
    <div class="panel-body">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th width="10%" class="text-center">Código</th>
					<th>Nome</th>
					<th width="10%" class="text-center">Alterar</th>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach ($lista_usuarios as $a)
				{
					echo '<tr>';
					echo "<td class=\"text-center\">{$a['i_usuario']}</td>";
					echo "<td>{$a['nome']}</td>";
					echo '<td class="text-center">';
					echo anchor('usuarios/alterar/'.$a['i_usuario'], '<span class="glyphicon glyphicon-pencil"></span>', array('class'=>'btn btn-default btn-xs', 'role'=>'button'));
					echo '</td>';
					echo '</tr>';
				}
			?>
			</tbody>
		</table>    
    </div>
</div>