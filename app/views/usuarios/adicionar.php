<div class="page-header">
    <h2>Cadastrar Usuário</h2>
</div>

<p>
    <?php echo anchor('usuarios/index', 'Voltar', array('class' => 'btn btn-default btn-sm')); ?>
</p>

<div class="panel panel-default">
    <div class="panel-body">

        <?php echo form_open('usuarios/salvar', '', array('i_empresa' => $this->session->userdata('i_empresa'))); ?>

            <div class="row">
                <div class="form-group col-md-1">
                    <label for="i_usuario" class="control-label">Código:</label>
                    <input type="input" class="form-control" id="i_usuario" name="i_usuario" value="<?php echo isset($i_usuario)?$i_usuario:''; ?>" readonly>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="nome" class="control-label">Nome:</label>
                    <input type="input" class="form-control" id="nome" name="nome" value="<?php echo isset($nome)?$nome:''; ?>" required autofocus>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="email" class="control-label">E-mail:</label>
                    <input type="input" class="form-control" id="email" name="email" value="<?php echo isset($email)?$email:''; ?>" required>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="senha" class="control-label">Senha:</label>
                    <input type="input" class="form-control" id="senha" name="senha" value="<?php echo isset($senha)?$senha:''; ?>" required autofocus>
                </div>
            </div>
            <?php if (isset($admin) && (int)$admin === 0): ?>
            <div class="row">
                <div class="form-group col-md-3">
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='A' name='status'>
                            <input type="checkbox" id="status" name="status" value="I" <?php echo isset($status) && $status === 'I'?'checked':''; ?>> Inativar
                        </label>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <button type="submit" class="btn btn-default">Salvar</button>

        <?php echo form_close(); ?>

    </div>
</div>
        