<script>
    $(function(){
        $("#tipo").change(function(){
            if (this.value == "U") {
                $(".linha_usuario").show();
                $(".linha_grupo").hide();
                $("#i_grupo").attr('disabled', true);
                $("#i_usuario_fixo").attr('disabled', false);
                $("#libera_usuario_fixo").attr('disabled', false);                
            } else if (this.value == "G") {
                $(".linha_usuario").hide();
                $(".linha_grupo").show();
                $("#i_grupo").attr('disabled', false);    
                $("#i_usuario_fixo").attr('disabled', true);
                $("#libera_usuario_fixo").attr('disabled', true);
            }
        });
    });
</script>
<div class="page-header">
    <h2>Permissões</h2>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <ol class="breadcrumb">
                    <li>Módulos</li>
                    <li>Menus</li>
                </ol>
            	<ul>
                <?php
                    foreach ($lista_menus as $key => $val) {
                        if (isset($val['menus'])) {
                            echo '<li class="dropdown">';
                            echo anchor('portal_permissoes/index/'.$key.'/', $val['nome']);
                            echo '<ul>';
                            foreach ($val['menus'] as $a) {
                                echo '<li>'.anchor('portal_permissoes/index/'.$key.'/'.$a['i_menu'], $a['nome']).'</li>';
                            }
                            echo '</ul>';
                            echo '</li>';
                        } else {
                            echo '<li>'.anchor('portal_permissoes/index/'.$key.'/', $val['nome']).'</li>';
                        }
                    }
                ?>
            	</ul>
            </div>
            <div class="col-md-6">
				<ol class="breadcrumb">
					<li>Permissões</li>
                    <?php
                        if (isset($obj_modulo))
                        {
                            echo "<li>{$obj_modulo['nome']}</li>";
					    }
                        if (isset($obj_menu))
                        {
                            echo "<li>{$obj_menu['nome']}</li>";
                        }
                    ?>
    			</ol>

		        <?php
                    if ((!empty($i_menu) && $i_menu!='NULL') || !empty($i_modulo)) {
                        echo form_open('portal_permissoes/salvar', '', array('i_empresa' => $this->session->userdata('i_empresa'), 'i_modulo' => $i_modulo, 'i_permissao' => '')+$input_menu);
                ?>
    		            <div class="row">
            		        <div class="form-group col-md-4">
                    		    <label for="tipo" class="control-label">Tipo:</label>
                        		<select id="tipo" name="tipo" class="form-control input-xs">
                                    <option value="">- Selecione -</option>
                                    <option value="U">Usuário</option>
                                    <option value="G">Grupo</option>
                                </select>
                    		</div>
                    	</div>
                    	<div class="row linha_grupo">
                            <div class="form-group col-md-10">
                                <label for="i_grupo" class="control-label">Grupo:</label>
                                <select id="i_grupo" name="i_grupo" class="form-control input-xs">
                                    <option value="">- Selecione -</option>
                                    <?php
                                        foreach ($lista_grupos as $a) {
                                            echo "<option value={$a['i_grupo']}>{$a['nome']}</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                    	</div>
                        <div class="row linha_usuario">
                            <div class="form-group col-md-10">
                                <label for="i_usuario_fixo" class="control-label">Usuário:</label>
                                <select id="i_usuario_fixo" name="i_usuario_fixo" class="form-control input-xs">
                                    <option value="">- Selecione -</option>
                                    <?php
                                        foreach ($lista_usuario as $a) {
                                            echo "<option value={$a['i_usuario']}>{$a['nome']}</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row linha_usuario">
                            <div class="form-group col-md-10">
                                <label for="libera_usuario_fixo" class="control-label">Permissão:</label>
                                <select id="libera_usuario_fixo" name="libera_usuario_fixo" class="form-control input-xs">
                                    <option value="L">Liberado</option>
                                    <option value="B">Bloqueado</option>
                                </select>
                            </div>
                        </div>                    
                		<button type="submit" class="btn btn-default">Salvar</button>
		       	<?php
                        echo form_close();
                    
                        if (count($lista_permissoes)>0) {
                ?>

				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th width="20%" class="text-center">Tipo</th>
							<th>Nome</th>
							<th width="15%" class="text-center">Status</th>
							<th width="15%" class="text-center">Remover</th>
						</tr>
					</thead>
					<tbody>
                        <?php
                            foreach ($lista_permissoes as $a) {
                                if ($a['tipo'] == 'U') {
                                    $tipo = 'Usuário';
                                    $acesso = $a['nome_usuario'];
                                    $libera_usuario_fixo = $a['libera_usuario_fixo'] === 'L'?'Liberado':'Bloqueado';
                                } else {
                                    $tipo = 'Grupo';
                                    $acesso = $a['nome_grupo'];
                                    $libera_usuario_fixo = 'Liberado';
                                }
                                
                                echo '<tr>';
                                echo "<td class=\"text-center\">{$tipo}</td>";
                                echo "<td>{$acesso}</td>";
                                echo "<td class=\"text-center\">{$libera_usuario_fixo}</td>";
                                echo "<td class=\"text-center\">";
                                echo anchor('portal_permissoes/deletar/'.$i_modulo.'/'.$i_menu.'/'.$a['i_permissao'], '<span class="glyphicon glyphicon-trash"></span>', array('class'=>'btn btn-default btn-xs', 'role'=>'button'));
                                echo "</td>";
                                echo '</tr>';
                            }
                        ?>
					</tbody>
				</table>
                <?php
                        }  
                    } else {
                        echo '<div class="alert alert-info" role="alert">';
                        echo 'Selecione um Item do Menu.</a>';
                        echo '</div>';
                    }

                ?>
            </div>
        </div>
    </div>
</div>