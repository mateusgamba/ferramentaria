<script>
function abrirModalEstoque(i_produto, descricao){
	$("#exampleModalLabel").html('Estoque: '+descricao);
	$("#exampleModal").modal('show');
}
</script>


<div class="page-header">
    <h2>Estoque</h2>
</div>

<div class="panel panel-default">
    <div class="panel-body">
    	<?php
    		if (count($lista)>0)
    		{
    	?>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th width="10%" class="text-center">Código</th>
					<th>Descrição</th>
					<th width="10%" class="text-center">Estoque</th>
					<th width="10%" class="text-center">Detalhes</th>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach ($lista as $a)
				{
					echo '<tr>';
					echo "<td class=\"text-center\">{$a['codigo']}</td>";
					echo "<td>{$a['nome']}</td>";
					echo '<td class="text-center">';
					echo '&nbsp;';
					echo "<button type=\"button\" class=\"btn\" onclick=\"abrirModalEstoque({$a['i_produto']}, '{$a['nome']}')\">detalhes</button>";
					echo '</td>';
					echo '<td class="text-center">';
					echo '&nbsp;';
					echo '</td>';				
					echo '</tr>';
				}
			?>
			</tbody>
		</table>
		<?php
			} else {
				echo '<br>';
				echo '<div class="alert alert-info" role="alert">';
				echo 'No momento não possui nenhum produto adicionado';
				echo '</div>';
			}
		?>
    </div>
</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel"></h4>
			</div>
			<div class="modal-body">
        		<form>
					<div class="row">
						<div class="form-group col-md-4">
							<div class="radio">
								<label>
									<input type="radio" name="tipo" id="tipoE" value="E" checked>
									Entrada no Estoque
								</label>
							</div>
						</div>
						<div class="form-group col-md-4">
							<div class="radio">
								<label>
									<input type="radio" name="tipo" id="tipoS" value="S">
									Saída do Estoque
								</label>
							</div>
						</div>
					</div>

					<div class="row">

						<div class="form-group col-md-4">
							<label for="message-text" class="control-label">Quantidade:</label>
							<input type="text" class="form-control" id="qtde" name="qtde">
						</div>

						<div class="form-group col-md-4">
							<label for="message-text" class="control-label">Valor:</label>
							<input type="text" class="form-control" id="valor" name="valor">
						</div>
					</div>

				</form>
      		</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary">Salvar</button>
			</div>
		</div>
	</div>
</div>