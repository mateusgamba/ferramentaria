<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ferramentaria :: Acesso</title>
        <?php
            echo link_tag('assets/css/bootstrap.min.css');
            echo link_tag('assets/css/signin.css');
        ?>        
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <section class="login-form">
                        <?php echo form_open('acesso/acessar', array('role'=>'login')); ?>
                            <img src="http://i.imgur.com/RcmcLv4.png" class="img-responsive" alt="">
                            <input type="email" name="email" placeholder="usuario@site.com" required class="form-control input-lg" autofocus>
                            <input type="password" name="senha" class="form-control input-lg" id="password" placeholder="Senha" required>
                            <?php echo validation_errors('<ul class="error-list"><li>', '</li></ul>'); ?>
                            <button type="submit" name="go" class="btn btn-lg btn-primary btn-block">Acessar</button>
                            <div>
                                <a href="#">Esqueceu sua senha?</a>
                            </div>
                        <?php echo form_close(); ?>
                        <div class="form-links">
                            <a href="#">www.ferramentaria.com</a>
                        </div>
                    </section>  
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </body>
</html>