<script>
$(function(){
	$('#my-select').multiSelect({
		selectableHeader: "<div class='custom-header'><strong>Usuários</strong></div>",
		selectionHeader: "<div class='custom-header'><strong>Usuários do Grupos</strong></div>"
	});
});
</script>
<style>
.ms-container {
	width: 100%;
}
</style>

<div class="page-header">
    <h2>Grupos de Usuários</h2>
</div>

<p>
	<?php echo anchor('grupos/index', 'Voltar', array('class' => 'btn btn-default btn-sm')); ?>
</p>

<div class="panel panel-default">
   
    <div class="panel-body">

        <?php echo form_open('grupos_usuarios/salvar', '', array('i_empresa' => $this->session->userdata('i_empresa'), 'i_usuario' => $this->session->userdata('i_usuario'), 'i_grupo' => $i_grupo)); ?>

    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<select multiple="multiple" id="my-select" name="usuarios[]">
				<?php
					foreach ($lista as $a)
					{
						$check = (int)$a['pertence'] === 1?'selected':'';
						echo "<option value=\"{$a['i_usuario']}\" {$check}>{$a['nome']}</option>";
					}
				?>
				</select>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="form-group text-center">
				<p><button type="submit" class="btn btn-default">Salvar</button></p>
			</div>
		</div>
		<?php echo form_close(); ?>
    </div>


</div>