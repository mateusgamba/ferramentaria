<div class="page-header">
    <h2>Alterar Dados Empresa</h2>
</div>

<div class="panel panel-default">
    <div class="panel-body">

        <?php echo form_open('empresas/salvar', '', array('i_empresa' => $this->session->userdata('i_empresa'))); ?>

            <div class="row">
                <div class="form-group col-md-1">
                    <label for="i_empresa" class="control-label">Código:</label>
                    <input type="input" class="form-control" id="i_empresa" name="i_empresa" value="<?php echo isset($i_empresa)?$i_empresa:''; ?>" readonly>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="nome" class="control-label">Nome:</label>
                    <input type="input" class="form-control" id="nome" name="nome" value="<?php echo isset($nome)?$nome:''; ?>" required autofocus>
                </div>
            </div>

            <button type="submit" class="btn btn-default">Salvar</button>

        <?php echo form_close(); ?>

    </div>
</div>
