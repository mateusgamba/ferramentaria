<div class="page-header">
    <h2>Menus</h2>
</div>

<p>
	<?php echo anchor('portal_modulos/index', 'Voltar', array('class' => 'btn btn-default btn-sm')); ?>
	<?php echo anchor('portal_menus/adicionar/'.$i_modulo, 'Adicionar', array('class' => 'btn btn-default btn-sm')); ?>
</p>

<div class="panel panel-default">
    <div class="panel-body">
    <?php
    	if (count($lista_menus)>0)
    	{
    ?>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th width="10%" class="text-center">Ordem</th>
					<th>Descrição</th>
					<th width="10%" class="text-center">Alterar</th>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach ($lista_menus as $a)
				{
					echo '<tr>';
					echo "<td class=\"text-center\">{$a['ordem']}</td>";
					echo "<td>{$a['nome']}</td>";
					echo '<td class="text-center">';
					echo anchor('portal_menus/alterar/'.$i_modulo.'/'.$a['i_menu'], '<span class="glyphicon glyphicon-pencil"></span>', array('class'=>'btn btn-default btn-xs', 'role'=>'button'));
					echo '</td>';
					echo '</tr>';
				}
			?>
			</tbody>
		</table>
	<?php
		}
		else
		{
			echo '<br>';
			echo '<div class="alert alert-info" role="alert">';
			echo 'No momento não possui nenhum menu adicionado, '.anchor('portal_menus/adicionar/'.$i_modulo, 'Clique aqui para Adicionar').'</a>';
			echo '</div>';
		}
	?>		
    </div>
</div>