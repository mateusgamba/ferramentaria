<div class="page-header">
    <h2>Cadastrar Menu</h2>
</div>

<p>
    <?php echo anchor('portal_menus/index/'.$i_modulo, 'Voltar', array('class' => 'btn btn-default btn-sm')); ?>
</p>

<div class="panel panel-default">
    <div class="panel-body">

        <?php echo form_open('portal_menus/salvar', '', array('i_empresa' => $this->session->userdata('i_empresa'), 'i_modulo' => $i_modulo)); ?>
            <div class="row">
                <div class="form-group col-md-1">
                    <label for="i_menu" class="control-label">Código:</label>
                    <input type="input" class="form-control" id="i_menu" name="i_menu" value="<?php echo isset($i_menu)?$i_menu:''; ?>" readonly>
                </div>
            </div>
            
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="nome" class="control-label">Nome:</label>
                    <input type="input" class="form-control" id="nome" name="nome" value="<?php echo isset($nome)?$nome:''; ?>" required autofocus>
                </div>
            </div>
            
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="link" class="control-label">Link:</label>
                    <input type="input" class="form-control" id="link" name="link" value="<?php echo isset($link)?$link:''; ?>" required>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-1">
                    <label for="ordem" class="control-label">Ordem:</label>
                    <input type="input" class="form-control" id="ordem" name="ordem" value="<?php echo isset($ordem)?$ordem:''; ?>" required>
                </div>
            </div>
            
            <div class="row">
                <div class="form-group col-md-3">
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='A' name='status'>
                            <input type="checkbox" id="status" name="status" value="I" <?php echo isset($status) && $status === 'I'?'checked':''; ?>> Inativar
                        </label>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-default">Salvar</button>
        
        <?php echo form_close(); ?>

    </div>
</div>
        