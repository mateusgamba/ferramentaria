<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.4/angular.min.js"></script>
<script>
	angular.module('app', []);
	angular.module('app').controller("MainController", function($scope){
		var vm = this;
		vm.produtos = <?php echo $lista; ?>;
		$scope.showModal = false;

	    $scope.abrirModalEstoque = function($i_produto, $nome, $valor){
	    	vm.nome_produto = $nome;
	    	vm.valor_produto = $valor;
	    	vm.i_produto = $i_produto;
			vm.qtde = '';
	        $scope.showModal = !$scope.showModal;
	    };

    	vm.addShow = function() {
    		for (i in vm.produtos) {
                if (vm.produtos[i].i_produto == vm.i_produto) {
                   vm.produtos[i].qtde = vm.new.qtde;
                   $scope.showModal = false;
                }
            }
    	};
	});

	angular.module('app').directive('modal', function () {
	    return {
	      template: '<div class="modal fade">' + 
	          '<div class="modal-dialog">' + 
	            '<div class="modal-content">' + 
	              '<div class="modal-header">' + 
	                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
	                '<h4 class="modal-title">{{main.nome_produto}}</h4>' + 
	              '</div>' + 
	              '<div class="modal-body" ng-transclude></div>' + 
	            '</div>' + 
	          '</div>' + 
	        '</div>',
			restrict: 'E',
			transclude: true,
			replace: true,
			scope: true,
			link: function postLink(scope, element, attrs) {
				scope.title = attrs.title;

				scope.$watch(attrs.visible, function(value){
				  if(value == true)
				    $(element).modal('show');
				  else
				    $(element).modal('hide');
				});

				$(element).on('shown.bs.modal', function(){
				  scope.$apply(function(){
				    scope.$parent[attrs.visible] = true;
				  });
				});

				$(element).on('hidden.bs.modal', function(){
				  scope.$apply(function(){
				    scope.$parent[attrs.visible] = false;
				  });
				});
	      	}
	    };
	});
</script>

<div ng-app="app" ng-controller="MainController as main">
	<div class="page-header">
	    <h2>Estoque</h2>
	</div>
	<div class="panel panel-default">
	    <div class="panel-body">

			<div class="row">
				<div class="form-group col-md-4">
					<label for="pesquisa_nome" class="control-label">Pesquisar Código/Nome:</label>
					<input type="text" class="form-control" id="pesquisa_nome" name="pesquisa_nome" ng-model="main.searchText">
				</div>
			</div>
	    	<?php if (count($lista)>0): ?>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th width="10%" class="text-center">Código</th>
						<th>Descrição</th>
						<th width="10%" class="text-center">Quantidade</th>
						<th width="10%" class="text-center">Valor</th>
						<th width="10%" class="text-center">Alterar</th>
						<th width="10%" class="text-center">Histórico</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="produto in main.produtos | filter:{codigo: smain.searchText, nome: main.searchText}">
						<td class="text-center">{{produto.codigo}}</td>
						<td>{{produto.nome}}</td>
						<td class="text-center">{{produto.qtde}}</td>
						<td class="text-center">{{produto.valor}}</td>
						<td class="text-center">
							<button type="button" class="btn btn-default btn-xs" ng-click="abrirModalEstoque(produto.i_produto, produto.nome, produto.valor)"><span class="glyphicon glyphicon-inbox"></span></button>
						</td>
						<td class="text-center">&nbsp;</td>
					</tr>
				</tbody>
			</table>
			<?php
				else:
					echo '<br>';
					echo '<div class="alert alert-info" role="alert">';
					echo 'No momento não possui nenhum produto adicionado';
					echo '</div>';
				endif;
			?>
	    </div>
	</div>
    
	<modal visible="showModal">
		<form name="main.addForm" class="form">
			<?php
				$data_i_produto = array(
				        'type'  => 'hidden',
				        'name'  => 'i_produto',
				        'id'    => 'i_produto',
				        'ng-model' => 'main.new.i_produto',
				        'value' => '{{main.i_produto}}'
				);

				$data_i_empresa = array(
				        'type'  => 'hidden',
				        'name'  => 'i_empresa',
				        'id'    => 'i_empresa',
				        'ng-model' => 'main.new.i_empresa',
				        'value' => $this->session->userdata('i_empresa')
				);
				echo form_input($data_i_empresa);

				$data_i_usuario = array(
				        'type'  => 'hidden',
				        'name'  => 'i_usuario',
				        'id'    => 'i_usuario',
				        'ng-model' => 'main.new.i_usuario',
				        'value' => $this->session->userdata('i_usuario')
				);
				echo form_input($data_i_usuario);
			?>
			<div class="row">
				<div class="form-group col-md-4">
					<div class="radio">
						<label>
							<input type="radio" name="tipo" ng-model="main.new.tipo" id="tipoE" value="E" checked>
							Entrada no Estoque
						</label>
					</div>
				</div>
				<div class="form-group col-md-4">
					<div class="radio">
						<label>
							<input type="radio" name="tipo" ng-model="main.new.tipo" id="tipoS" value="S">
							Saída do Estoque
						</label>
					</div>
				</div>
			</div>

			<div class="row">

				<div class="form-group col-md-4">
					<label for="message-text" class="control-label">Quantidade:</label>
					<input type="text" class="form-control" id="qtde" name="qtde" ng-model="main.new.qtde" value="{{main.qtde}}">
					<input type="hidden" class="form-control" id="i_produto" name="i_produto" ng-model="main.new.i_produto">
				</div>

				<div class="form-group col-md-4">
					<label for="message-text" class="control-label">Valor:</label>
					<input type="text" class="form-control" id="valor" name="valor" value="{{main.valor_produto}}"  >
				</div>
			</div>
		<?php echo form_close(); ?>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
			<button type="button" class="btn btn-primary" ng-click="main.addShow()">Salvar</button>
		</div>
	</modal>
</div>