<div class="page-header">
    <h2>Locação</h2>
</div>

<p>
	<?php echo anchor('locacoes/adicionar', 'Adicionar', array('class' => 'btn btn-default btn-sm')); ?>
</p>

<div class="panel panel-default">
    <div class="panel-body">
    <?php
    	if (count($lista_locacoes)>0)
    	{
    ?>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th width="10%" class="text-center">Retirada</th>
					<th width="10%" class="text-center">Previsão</th>
					<th>Usuário</th>
					<th width="10%" class="text-center">Detalhes</th>
					<th width="10%" class="text-center">Baixar</th>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach ($lista_locacoes as $a)
				{
					echo '<tr>';
					echo "<td class=\"text-center\">{$a['dt_retirada']}</td>";
					echo "<td class=\"text-center\">{$a['dt_prevista']}</td>";
					echo "<td>{$a['nome_usuario']}</td>";
					echo "<td>Detalhes</td>";
					echo "<td>Baixar</td>";
					echo '</tr>';
				}
			?>
			</tbody>
		</table>
	<?php
		}
		else
		{
			echo '<br>';
			echo '<div class="alert alert-info" role="alert">';
			echo 'No momento não possui nenhum locação adicionado, '.anchor('locacoes/adicionar', 'Clique aqui para Adicionar').'</a>';
			echo '</div>';
		}
	?>
    </div>
</div>