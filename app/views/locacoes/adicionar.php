<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 34px;
}
.select2-container .select2-selection--single {
    height: 34px;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 34px;
}
</style>
<script>
$(document).ready(function() {
   $('#datetimepicker1').datetimepicker({
        sideBySide: true,
        locale: 'pt-BR',
        showTodayButton: true,
        showClose: true
    });

    $("#i_usuario").select2({
        placeholder: "Buscar",
        minimumInputLength: 3,
        ajax: {
            url:"busca_usuarios",
            dataType: 'json',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, //search term
                    page_limit: 10, // page size
                    page: page
                };
            },
            results: function (data, page) {
                console.log(data);
                var more = (page * 10) < data.total;
                return {results: data.results, more: more};
            }
        },
        escapeMarkup: function (m) { return m; }
    });
});

</script>
<div class="page-header">
    <h2>Cadastrar Locação</h2>
</div>

<p>
    <?php echo anchor('locacoes', 'Voltar', array('class' => 'btn btn-default btn-sm')); ?>
</p>

<div class="panel panel-default">
    <div class="panel-body">
    	<?php echo form_open('portal_modulos/salvar', '', array('i_empresa' => $this->session->userdata('i_empresa'))); ?>

            <div class="row">
                <div class="form-group col-md-1">
                    <label for="i_locacao" class="control-label">Código:</label>
                    <input type="input" class="form-control" id="i_locacao" name="i_locacao" value="<?php echo isset($i_locacao)?$i_locacao:''; ?>" readonly>
                </div>

                <div class="form-group col-md-2">
 
            <!--div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div-->
                    <label for="dt_retirada" class="control-label">Data Retirada:</label>
                    <div class="input-group date" id="datetimepicker1">
                        <input type="input" data-format="dd/MM/yyyy hh:mm:ss" class="form-control" id="dt_retirada" name="dt_retirada" value="<?php echo isset($dt_retirada)?$dt_retirada:''; ?>">
                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        
                    </div>
                </div>

                <div class="form-group col-md-2">
                    <label for="dt_prevista" class="control-label">Data Prevista Entrega:</label>
                    <input type="input" class="form-control" id="dt_prevista" name="dt_prevista" value="<?php echo isset($dt_prevista)?$dt_prevista:''; ?>">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-5">
                    <label for="i_usuario" class="control-label">Código/Nome Usuário:</label>
                    <select id="i_usuario" name="i_usuario" class="form-control input-lg">
                    </select>                    
                </div>
            </div>

            <hr>
    	<?php echo form_close(); ?>
    </div>
</div>