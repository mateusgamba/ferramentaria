<?php
    $lista_menus = get_menus($this->session->userdata('i_usuario'));
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>iF - Ferramentas</title>
        <?php
            echo link_tag('assets/css/bootstrap.min.css');
            echo link_tag('assets/css/bootstrap-theme.min.css');
            echo link_tag('assets/css/bootstrap-datetimepicker.min.css');
            echo link_tag('assets/css/style.css');
            echo '<!--[if lt IE 9]>';
            echo link_script('assets/js/ie8-responsive-file-warning.js');
            echo '<![endif]-->';
            echo link_script('assets/js/ie-emulation-modes-warning.js');
            echo '<!--[if lt IE 9]>';
            echo link_script('assets/js/html5shiv.min.js');
            echo link_script('assets/js/respond.min.js');
            echo '<![endif]-->';

            echo link_tag('assets/css/multi-select.css');

            echo link_script('assets/js/jquery.min.js');

            echo link_script('assets/js/ie10-viewport-bug-workaround.js');
            echo link_script('assets/js/jquery.multi-select.js');
            echo link_script('assets/js/moment-with-locales.js');
            echo link_script('assets/js/bootstrap-datetimepicker.min.js');
            
        ?>
    </head>
    
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">iF</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <?php
                            foreach ($lista_menus as $key => $val) {
                                if (isset($val['menus'])) {
                                    $aux_menu = 0;
                                    echo '<li class="dropdown">';
                                    echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'.$val['nome'].' <span class="caret"></span></a>';
                                    echo '<ul class="dropdown-menu" role="menu">';
                                    foreach ($val['menus'] as $a) {
                                        echo $aux_menu === 1?'<li class="divider"></li>':'';
                                        echo '<li>'.anchor($a['link'], $a['nome']).'</li>';
                                        $aux_menu = 1;
                                    }
                                    echo '</ul>';
                                    echo '</li>';
                                } else {
                                    echo '<li>'.anchor($val['link'], $val['nome']).'</a></li>';
                                }
                            }
                        ?>
                        <li><a href="#contact"><?php echo $this->session->userdata('nome'); ?></a></li>
                        <li><?php echo anchor('acesso/sair', 'Sair'); ?></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
        	<?php echo $contents ?>
        </div>

    </body>
</html>
