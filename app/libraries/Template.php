<?php

class Template {

    protected $CI;
	public $template_data = array();

    public function __construct()
    {
		$this->CI =& get_instance();
    }

 	public function set($name, $value)
	{
		$this->template_data[$name] = $value;
	}
 
 	public function get()
	{
		return $this->template_data;
	}

	public function load($view = '', $view_data = '')
	{               
		$this->CI =& get_instance();
		$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));
		return $this->CI->load->view('template', $this->get(), false);
	}
}