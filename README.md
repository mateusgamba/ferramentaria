# Projeto Ferramentaria #

Projeto tem como objetivo de desenvolver um sistema para controle de ferramentas

### Instalação ###

1) Clonar projeto

```
#!php

git clone https://www.bitbucket.org/mateusgamba/ferramentaria
```

2) Criar o banco de dados no MySQL

```
#!sql

create database ferramentaria;
```

3) Executar o script db.sql que se encontra na raiz do projeto.

```
#!sql

mysql -u username -p password ferramentaria < db.sql
```

4) Configurar aplicação

app/config/config.php

app/config/database.php


5) Usuário de acesso Full

Usuário: root@root.com

Senha: 123123
